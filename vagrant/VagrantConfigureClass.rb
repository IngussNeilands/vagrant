
require 'yaml'

class VagrantConfigureClass

    @@vagrantServer = {}
    @@defaults = {}

    def initialize()

        @@defaults =  YAML.load_file("vagrant/VagrantConfig.yml")

        Dir.glob("**/Vagrantfile.yml", File::FNM_CASEFOLD) do |customConfigFile|
            puts "Included Vagrant Config: ./#{customConfigFile}"
            customConfig = YAML.load_file(customConfigFile)
            if !customConfig['server'].nil?
                customConfig['server'].each do |vmID, vmServer|
                    @@vagrantServer[vmID] = vmServer
                    @@vagrantServer[vmID]['id'] = vmID
                    @@vagrantServer[vmID]['hostname'] = vmServer['hostname'] ||= 'Sandbox_' + vmID.slice(0,1).capitalize + vmID.slice(1..-1)
                    if !customConfig['config'].nil?
                        customConfig['config'].each do |config, configHash|  @@vagrantServer[vmID][config] = @@vagrantServer[vmID][config] ||= configHash end
                    end
                    @@defaults['config'].each do |config, configHash|  @@vagrantServer[vmID][config] = @@vagrantServer[vmID][config] ||= configHash end
                end
            end
        end

        if @@vagrantServer.empty?
            puts "Using Default Vagrant Config, Please replace!"
            @@defaults['server'].each do |vmID, vmServer|
                @@vagrantServer[vmID] = vmServer
                @@vagrantServer[vmID]['id'] = vmID
                @@vagrantServer[vmID]['hostname'] = vmServer['hostname'] ||= 'Sandbox_' + vmID.slice(0,1).capitalize + vmID.slice(1..-1)
                @@defaults['config'].each do |config, configHash|  @@vagrantServer[vmID][config] = @@vagrantServer[vmID][config] ||= configHash end
            end
        end

        #puts JSON.pretty_generate( @@vagrantServer )
        #exit()

    end

    def getVagrantServer()
        return @@vagrantServer
    end

end
