# Insecure Keypair

These keys are the "insecure" public/private keypair Vagrant offer to
[base box creators](http://docs.vagrantup.com/v2/boxes/base.html) for use in their base boxes so that
vagrant installations can automatically SSH into the boxes.

# SSH Access

User: vagrant
Password: vagrant
Key: ssh.key.vagrant.ppk
