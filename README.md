# Vagrant Sandbox #

Fine Tuned Vagrant, for use with these boxes -> [hashicorp/IngussNeilands](https://atlas.hashicorp.com/IngussNeilands)

Automatically triggers Service Restart after Vagrant Mounts Folders

### Configuration and Setup ###
* Checkout, Vagrant up, Enjoy!

### Customization, Multiple Configs ###

* Copy Vagrantfile.Simple.yml to Vagrantfile.yml

* Add Vagrantfile.yml to your Project subdirectory within Vagrant root

* Run: Vagrant Status

* Enjoy

### Custom Tweaks ###

* Read Contents of Vagrantfile.Custom.yml
* You may use any box and provision as you like
* Sample included

### Advanced Tweaks ###

* Read Contents of Vagrantfile.Advanced.yml
* Modify your Vagrantfile.yml as necessary

### Whom Should I Contact? ###

* If You Need Additional Boxes
* Have Some Questions
* Wanna Send Thanks

Use this email: **IngussNeilands [at] gmail [dot] com**