# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrant Default Provider
#
ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'

# Vagrant Command Arg
#
vagrantCMD = ARGV[0]

# Vagrant Config
#
require_relative  'vagrant/VagrantConfigureClass.rb'
VagrantConfigure = VagrantConfigureClass.new

# Vagrant
#
Vagrant.configure('2') do |config|
    VagrantConfigure.getVagrantServer.each_with_index do | ( vmServerID, vmServer ), index |
        config.vm.define vmServerID, 0 === index ? { primary: true } : { autostart: false } do |server|


            if vmServer["box_url"] != nil then server.vm.box_url = vmServer["box_url"] end
            #server.vm.hostname = vmServer["hostname"]
            server.vm.box = vmServer["box"]

            server.vm.box_check_update = vmServer["vm.box_check_update"]
            server.vm.boot_timeout = vmServer["vm.boot_timeout"].to_i

            server.ssh.forward_agent = vmServer["vm.ssh.forward_agent"]
            server.ssh.insert_key = vmServer["vm.ssh.insert_key"]
            server.ssh.shell = vmServer["vm.ssh.shell"].to_s

            server.vm.provider 'virtualbox' do |virtualbox|
                if !vmServer["vm.provider.virtualbox.customize"].nil?
                    vmServer["vm.provider.virtualbox.customize"].each do |customizeKey, customizeValue| 
                        virtualbox.customize ['modifyvm', :id, customizeKey, customizeValue]
                    end
                    virtualbox.gui = vmServer["vm.provider.virtualbox.gui"]
                end
            end

            if !vmServer["vm.network.forwarded_port"].nil?
                vmServer["vm.network.forwarded_port"].each do |portKey, portValue| 
                    server.vm.network 'forwarded_port', guest: portKey.to_s, host: portValue.to_s, auto_correct: true
                end
            end

            if !vmServer["vm.synced_folder"].nil?
                vmServer["vm.synced_folder"].each do |folderKey, folderValue|
                    if folderValue["from"] && folderValue["to"] then
                        server.vm.synced_folder folderValue["from"], folderValue["to"], :create => 'true',
                            owner: folderValue["owner"] ||= "root",
                            group: folderValue["group"] ||= "root",
                            mount_options: [folderValue["mount_options"] ||= "dmode=777,fmode=666"]
                    end
                end
            end

            if !vmServer["vm.shell.provision"].nil?
                    vmServer["vm.shell.provision"].each do |inlineScript|
                        server.vm.provision :shell, :inline => inlineScript
                    end
            end

            if !vmServer["vm.shell"].nil?
                vmServer["vm.shell"].each do |inlineScript|
                    server.vm.provision :shell, :inline => inlineScript, run: "always"
                end
            end

            #puts JSON.pretty_generate( vmServer )
            #exit()

        end
    end
end
